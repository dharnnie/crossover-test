var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var SRC_DIR = path.resolve(__dirname, 'src');
var DIST_DIR = path.resolve(__dirname, 'dist');

module.exports = {
  devtool: 'source-map',
  entry: SRC_DIR + '/app/app.js',
  output:{
    path: DIST_DIR,
    filename: 'bundle.js',

  },
  module:{
    rules:[
      {
        test: /\.js$/,
        use:{
          loader: 'babel-loader',
          options:{
            presets:['env']
          }
        },
      },
      {
        test: /\.css$/,
        use:['style-loader', 'css-loader']
      },
      {
        test:/\.(png|jpg|svg)$/,
        use:{
          loader: 'file-loader',
        }
      }
    ]
  },
  plugins:[
    new HtmlWebpackPlugin({
      title: "Crossover Video Player",
      template: SRC_DIR + "/index.ejs"
    })
  ],
  devServer:{
    contentBase: DIST_DIR,
    historyApiFallback: true
  },
  resolve:{
    alias:{
      app: path.resolve(__dirname, 'src/app/'),
    }
  }
}
