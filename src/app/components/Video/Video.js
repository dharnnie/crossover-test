import React,{Component} from 'react'
import PropTypes from 'prop-types'
import CrossoverNavigation from 'app/components/Nav/CrossoverNavigation'

import './Video.css'
//import video from './video.mp4'


class Video extends Component {
  constructor(props) {
    super(props);
  }
  render(){
    {/*dest checks for the destination of the Video Component.
      sets appropriate className for display based on destination
      - Destination can be, all-videos or single video
    */}
    var dest = new String()
    //let dest = this.props.destination === 'all-videos'? 'col-lg-3': 'col-lg-12'
    if (this.props.destination === 'all-videos'){
      dest = 'col-lg-3'
    }else if(this.props.destination === 'all-in-watch'){
      dest = 'col-lg-4'
    }else{
      dest = 'col-lg-12'
    }
    return(
        <div className={dest}>
          <div className="panel">
            <div className="panel-body">
              <video type="video/mp4" width="100%" height="" controls>
                <source src=""/>
              </video>
              <p>The ideas above can be wrapped in a small helper that can be incorporated into the book project. To get started, install the dependencies</p>
              <small>Rate</small>
              <div>
                  <ul className="rating pagination pagination-sm pull-left">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                  </ul>
                </div>
            </div>
          </div>
        </div>
    );
  }
}
Video.propTypes = {
  destination: PropTypes.string
}
export default Video
