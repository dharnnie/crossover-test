import React,{Component} from 'react';
import {authenticateUser} from 'app/api-utils/auth';
import authStore from 'app/redux/stores/authStore';
import constants from 'app/redux/constants';
import {weHaveCompany,signIn} from 'app/redux/actions/actions';

import LoginForm from 'app/components/Login/LoginForm'
//In@!r49....
class LoginContainer extends Component {
  constructor(){
    super();
    authStore.dispatch(weHaveCompany(false));// dispatch action to tell state we have a user
    this.state = {
      userState: authStore.getState().signedIn,// local (// true or false) state of the container component to be passed down as props
      authing: false
    }
  }
  componentDidMount(){
    this.unsubscribe = authStore.subscribe(()=>// initialize state as signedIn=false
      this.setState({userState:authStore.getState().signedIn})
    );
  }
  componentWillUnmount(){
    this.unsubscribe()
  }

  async validateUser(username, password){
    //this.setState((state)=>{authing:true});
    var result = {};
    const res = await authenticateUser(username, password).then((response)=> response);
    //validate user from API call
    console.log('response from await authenticateUser: ',res);
    if (res.status === 'success'){
      // dispatch action to change userState to signed in
      // nb: signIn is an action creator and it puts all args in a single object
      authStore.dispatch(signIn(true,res.sessionId,res.user));
      result = {
        sessionId: res.sessionId,
        user: res.username,
        authState: true
      }
    }else if(res.status === 'error'){
      console.log('Error from API');
      // update the appState in authStore
      authStore.dispatch(weHaveCompany(false));
      result = {
        sessionId: '',
        user: '',
        authState: false
      }
    }else {
      console.log('This is a wierd behaviour')
    }
    return result;
  }
  render(){
    return(
      <LoginForm
        userState={this.state.userState}
        auth={this.validateUser}>
      </LoginForm>
    );
  }
}

export default LoginContainer;
