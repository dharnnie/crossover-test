//Node module imports first
import 'whatwg-fetch';
import React,{Component} from 'react';
import PropTypes from 'prop-types';
import styles from './Login.css';
import logo from './assets/crossover-logo.png';
import {withRouter} from 'react-router-dom';

class LoginForm extends Component{
  constructor(props){
    super(props);
    this.state = { // Use local state for form validation
      uNameField: "",
      pwdField: "",
      formErr:"",
      authing: false,
      loginErr: "",
    };
  }
  handleChange(event){ // Responds to changes in form fields
    this.setState({
      [event.target.id]: event.target.value
    });
  }
  createSession(id,username){ // creates a session for user on succesful authentication
    localStorage.setItem('loggedIn','loggedIn');
    localStorage.setItem('sessionId',id);
    localStorage.setItem('username',username);
    console.log('session created')
  }
  //Gets form data, validates calls the auth prop from container to
  async handleSubmit(event){
    event.preventDefault();
    this.setState({
      authing:true
    })
    if(this.state.uNameField === '' || this.state.pwdField === ''){
      console.log('One or more fields empty')
      this.setState({formErr:"Either field can't be empty"});
      console.log(this.state.formErr)
      this.setState({
        authing:false
      })
    }else {
      this.setState({
        authing:true
      })
      var authIsDone = await this.props.auth(this.state.uNameField,this.state.pwdField)
      console.log("Result of auth from LoginForm:01 ",authIsDone)
      if (authIsDone.authState){
        console.log("Result of auth from LoginForm: ",authIsDone)
        this.createSession(authIsDone.sessionId, authIsDone.user)
        this.setState({isLoggedIn:true});
        this.props.history.push('/videos')
        //decide if to redirect or not
      }else{
        this.setState({loginErr:"error occured"})
      }
    }
  }
  render(){
    let iconAnimation = !this.state.authing ? <i className="fa fa-user-circle"></i>:<i className="fa fa-spin fa-user-circle"></i>
    let formValidationErr = !this.state.formErr === ''? this.state.formErr:'';
    let authMessage = !this.state.loginErr === '' ? 'Opps... Something went wrong, we could not sign you on':'';
    return(
      <div className="back">
        <div className="container">
          <form onSubmit={this.handleSubmit.bind(this)} className="login-form">
            <div className="login-wrap">
              <p className="login-img">{iconAnimation}</p>
              {/*<img src={logo}/>*/}
              <small className="login-error">{authMessage}</small>
              <small className="login-error">{formValidationErr}</small>
              <div className="input-group">
                <span className="input-group-addon"><i className="fa fa-user-circle"></i></span>
                <input id="uNameField" value={this.state.username} onChange={this.handleChange.bind(this)} type="text" className="form-control" placeholder="Username" autoFocus/>
              </div>
              <div className="input-group">
                  <span className="input-group-addon"><i className="fa fa-key"></i></span>
                  <input value={this.state.password} onChange={this.handleChange.bind(this)} id="pwdField" type="password" className="form-control" placeholder="Password"/>
              </div>
              <label className="checkbox">
                  <span className="pull-right"> <a>Forgot Password?</a></span>
              </label>
              <button className="btn btn-primary btn-lg btn-block" type="submit">Login</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

LoginForm.propTypes={
  auth: PropTypes.func.isRequired, // recieves username and password in Login for authentication
}
export default withRouter(LoginForm);
