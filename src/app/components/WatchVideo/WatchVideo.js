import React,{Component} from 'react';
import PropTypes from 'prop-types';
import Video from 'app/components/Video/Video';
import Videos from 'app/components/Videos/Videos';


class WatchVideo extends Component {
  render(){
    return(
      <div className="row wrapper">
        <div className="col-lg-12">
          <Video destination=""></Video>
          <div className="">
            <Videos destination="watch-video" eachVideoDestination='all-in-watch'></Videos>
          </div>
        </div>
      </div>
    );
  }
}

WatchVideo.propTypes = {
  video:PropTypes.string
}

export default WatchVideo;
