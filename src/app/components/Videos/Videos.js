import React,{Component} from 'react';
import PropTypes from 'prop-types';
import './Videos.css'

import Video from 'app/components/Video/Video';



class Videos extends Component {
  constructor(props){
    super(props);
  }
  componentWillMount(){
    //fetch videos
  }
  render(){
    {/*
      videosClassName helps us set a className based on where it will be rendered
      we get the value from this.props.destination and we then decide what css class it should get with the value
    */}
    let videosClassName = this.props.destination === "watch-video" ? "col-lg-12": "col-lg-12"
    return(
      <div className="row wrapper">
        <div className={videosClassName}>
          <Video destination={this.props.eachVideoDestination}/>
          <Video destination={this.props.eachVideoDestination}/>
          <Video destination={this.props.eachVideoDestination}/>
          <Video destination={this.props.eachVideoDestination}/>
        </div>
      </div>
    );
  }
}
Videos.propTypes = {
  destination: PropTypes.string.isRequired, //
  eachVideoDestination: PropTypes.string
}
export default Videos
