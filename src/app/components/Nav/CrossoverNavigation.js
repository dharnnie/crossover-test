import {Button, Navbar, Nav,NavItem}  from 'react-bootstrap';
import React,{Component} from 'react';

class CrossoverNavigation extends Component{
  render(){
    return(
      <Navbar inverse fluid collapseOnSelect >
        <Navbar.Header>
          <Navbar.Brand>
            <a href="#">CVPlayer</a>
            <Navbar.Toggle/>
          </Navbar.Brand>
        </Navbar.Header>
      </Navbar>
    );
  }
}

export default CrossoverNavigation;
