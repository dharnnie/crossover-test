import {createStore} from 'redux';
import authReducer from 'app/redux/reducers/authReducer';


const authStore = createStore(authReducer);

export default authStore;
