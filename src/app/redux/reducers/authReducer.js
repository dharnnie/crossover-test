import constants from 'app/redux/constants';
import expect from 'expect';

const initialState = { //appState represents the initial state before logging in
  signedIn: false,
  sessionId: '',
  user: ''
}

const authReducer = (state = initialState, action) =>{
  switch (action.type) {
    case constants.WE_HAVE_COMPANY:
      console.log(constants.WE_HAVE_COMPANY);
      return Object.assign({},state,{
        signedIn:action.signedIn
      });
    case constants.LOGIN_AUTH:
      console.log(constants.LOGIN_AUTH);
      return Object.assign({},state,{
        signedIn:action.signedIn,
        sessionId: action.sessionId,
        user: action.user
      });
    default:
      return state;
  }
}




const testAuthReducer = () =>{
  expect(
    authReducer({signedIn:false},{type:constants.LOGIN_AUTH})
  ).toEqual({signedIn:false});

  expect(
    authReducer({signedIn:true},{type:constants.LOGIN_AUTH})
  ).toEqual({signedIn:true})
}

export default authReducer;
