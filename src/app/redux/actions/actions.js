import constants from 'app/redux/constants'

export function weHaveCompany(signedIn){ // initilizes application state as signed out when fired
  return {
    type: constants.WE_HAVE_COMPANY,
    signedIn
  }
}

export function signIn(signedIn,sessionId,user){// authenticates
  return{
    type: constants.LOGIN_AUTH,
    signedIn,
    sessionId,
    user
  }
}


export default {weHaveCompany,signIn};
