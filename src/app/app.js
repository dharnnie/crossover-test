import {render} from 'react-dom';
import React,{Component} from 'react';
import './index.css';
import {BrowserRouter, Route} from 'react-router-dom';

import CrossoverNavigation from 'app/components/Nav/CrossoverNavigation';
import LoginContainer from 'app/components/Login/LoginContainer';
import Videos from 'app/components/Videos/Videos';
import Video from 'app/components/Video/Video';
import WatchVideo from 'app/components/WatchVideo/WatchVideo';

class App extends Component{
  render(){
    return(
      <BrowserRouter>
        <div>
          <CrossoverNavigation/>
          <Route exact path="/" component={LoginContainer}/>
          <Route path="/watch" component={WatchVideo}/>
          <Route path="/videos" component={Videos}/>
        </div>
      </BrowserRouter>
    );
  }
}

render(<App/>, document.getElementById("root"));
