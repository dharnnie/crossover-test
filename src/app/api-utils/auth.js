// Handles the api call for Authentication
//import 'whatwg-fetch';
import axios from 'axios';


const BASE_URL = 'http://localhost:3000';
const API_HEADERS = {
      'Accept': 'application/json',
      'Content-type': 'application/json'
}

function authenticateUser(username,password){
  var data = {
    'username':username,
    'password': password
  }
  return axios.post(`${BASE_URL}/user/auth`,data,API_HEADERS).then((response)=> response.data)
}
export {authenticateUser}
